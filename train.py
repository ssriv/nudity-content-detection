import sys

import numpy as np
import argparse
import glob
import tensorflow as tf
import pandas as pd

from sklearn.utils import shuffle

from model import NudityDetectionModel, InputType
from utils.image_utils import create_tensorflow_image_loader
from utils.image_utils import create_yahoo_image_loader

IMAGE_LOADER_TENSORFLOW = "tensorflow"
IMAGE_LOADER_YAHOO = "yahoo"

evaluation_report = pd.DataFrame([])
loss_trace = pd.DataFrame([])

def create_batch_iterator(filenames,labels, batch_size, fn_load_image):
    for i in range(0, len(filenames), batch_size):
        ret = [list(map(fn_load_image, filenames[i:i+batch_size])),labels[i:i+batch_size]]
        yield ret


def create_tf_batch_iterator(filenames,labels,batch_size):
    for i in range(0, len(filenames), batch_size):
        with tf.Session(graph=tf.Graph()) as session:
            fn_load_image = create_tensorflow_image_loader(session,
                                                           expand_dims=False)
            yield [list(map(fn_load_image, filenames[i:i+batch_size])),labels[i:i+batch_size]]

def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--source",
                        help="Parent directory for safe and unsafe images",
                        default="dataset/sample_set/")
    parser.add_argument("-m", "--model_weights",
                        help="Path to trained model weights file",
                        default="model_weigths/open_nsfw-weights.npy")

    parser.add_argument("-b", "--batch_size", help="Number of images to \
                        classify simultaneously.", type=int, default=64)

    parser.add_argument("-l", "--image_loader",
                        default=IMAGE_LOADER_YAHOO,
                        help="image loading mechanism",
                        choices=[IMAGE_LOADER_YAHOO, IMAGE_LOADER_TENSORFLOW])

    args = parser.parse_args()
    batch_size = args.batch_size

    input_type = InputType.TENSOR
    model = NudityDetectionModel()

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    all_safe_file_name = glob.glob(args.source+"/"+"safe/*.jpg")
    all_unsafe_file_name = glob.glob(args.source + "/" + "unsafe/*.jpg")

    len_safe = len(all_safe_file_name)
    len_unsafe = len(all_unsafe_file_name)
    split_ratio = .80

    all_safe_file_name = shuffle(all_safe_file_name,random_state=5)
    all_unsafe_file_name = shuffle(all_unsafe_file_name,random_state=6)

    train_safe_images = all_safe_file_name[0:int(len_safe*split_ratio)]
    val_safe_images = all_safe_file_name[int(len_safe*split_ratio):len_safe]

    train_unsafe_images = all_unsafe_file_name[0:int(len_unsafe*split_ratio)]
    val_unsafe_images = all_unsafe_file_name[int(len_unsafe*split_ratio):len_unsafe]

    train_safe_label = list(np.zeros(len(train_safe_images)))
    val_safe_label = list(np.zeros(len(val_safe_images)))

    train_unsafe_label = list(np.ones(len(train_unsafe_images)))
    val_unsafe_label = list(np.ones(len(val_unsafe_images)))

    train_final_image = train_safe_images + train_unsafe_images
    val_final_image = val_safe_images + val_unsafe_images

    train_final_label = train_safe_label + train_unsafe_label
    val_final_label = val_safe_label + val_unsafe_label

    train_final_image, train_final_label = shuffle(train_final_image, train_final_label,random_state=0)
    val_final_image, val_final_label = shuffle(val_final_image, val_final_label,random_state=0)


    with tf.Session(graph=tf.Graph(), config=config) as session:
        #all layers will be trained. For training some layers, make code level changes in the argument.
        model.build(weights_path=args.model_weights,
                    input_type=input_type,trainable=True)
        session.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
        training_loss = []
        eval_stats = []
        Max_Epoch = 10
        loss_csv_counter = 0
        for epoch_num in range(Max_Epoch+1):
            if args.image_loader == IMAGE_LOADER_TENSORFLOW:
                batch_iterator = create_tf_batch_iterator(train_final_image, train_final_label, batch_size)
            else:
                fn_load_image = create_yahoo_image_loader(expand_dims=False)
                batch_iterator = create_batch_iterator(train_final_image, train_final_label, batch_size,
                                                       fn_load_image)
            for batch_num,output in enumerate(batch_iterator):
                images = output[0]
                labels = output[1]
                _,loss = session.run([model.training,model.loss],feed_dict={model.input: images,model.label:labels})
                training_loss.append([epoch_num,batch_num,loss])
                if(batch_num%5==0):
                    print("Epoch ",epoch_num," | Batch ",batch_num,"| Loss",loss)
                    loss_trace.loc[loss_csv_counter, "epoch_num"] = epoch_num
                    loss_trace.loc[loss_csv_counter, "batch_num"] = batch_num
                    loss_trace.loc[loss_csv_counter, "loss"] = loss
                    loss_csv_counter += 1
            accuracy, precision, recall, f1 = evaluate(args,model,val_final_image,val_final_label)
            eval_stats.append([epoch_num,accuracy, precision, recall, f1])
            print("Epoch",epoch_num,"accuracy",accuracy,"precision",precision,"recall",recall,"f1",f1)

            evaluation_report.loc[epoch_num, "Epoch"] = epoch_num
            evaluation_report.loc[epoch_num, "accuracy"] = accuracy
            evaluation_report.loc[epoch_num, "precision"] = precision
            evaluation_report.loc[epoch_num, "recall"] = recall
            evaluation_report.loc[epoch_num, "f1"] = f1
            name = "training_results/NSFW_Weigts" + str(epoch_num) + "/model.ckpt"
            save_path = saver.save(session, name)
            print("Model saved in path: %s" % save_path)
        loss_trace.to_csv("training_results/loss_trace.csv", index=False)
        evaluation_report.to_csv("training_results/evaluation_report.csv", index=False)

#Accuracy and loss calculation
def evaluate(args,model,val_final_image,val_final_label,batch_size=64):

    tTP,tTN,tFP,tFN = 0,0,0,0

    if args.image_loader == IMAGE_LOADER_TENSORFLOW:
        batch_iterator = create_tf_batch_iterator(val_final_image, val_final_label, batch_size)
    else:
        fn_load_image = create_yahoo_image_loader(expand_dims=False)
        batch_iterator = create_batch_iterator(val_final_image, val_final_label, batch_size,
                                               fn_load_image)
    sess = tf.get_default_session()
    for batch_num, output in enumerate(batch_iterator):
        images = output[0]
        labels = output[1]
        cTP,cTN,cFP,cFN = sess.run([model.TP,model.TN,model.FP,model.FN],feed_dict={model.input:images,model.label:labels})
        tTP += cTP
        tTN += cTN
        tFP += cFP
        tFN += cFN
    print("tTP",tTP,"tTN",tTN,"tFP",tFP,"tFN",tFN)
    accuracy = (tTP+tTN) / (tTP+tTN+tFP+tFN)
    precision = (tTP) / (tTP+tFP)
    recall = (tTP) / (tTP+tFN)
    f1 = 2*(precision*recall)/(precision+recall)
    return (accuracy,precision,recall,f1)

if __name__ == "__main__":
    main(sys.argv)