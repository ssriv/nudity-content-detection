import math
import os
import re
import subprocess

import cv2
import imutils


def extract_frames_from_video(path_to_video, required_frames_per_sec):
    rotation_angle = find_video_rotation__(path_to_video)
    total_frames_created = 0
    videoFile = path_to_video
    video_name = path_to_video.split("/")[-1].split(".")[0]

    cap = cv2.VideoCapture(videoFile)  # capturing the video from the given path
    num_frame = cap.get(7)  # number of frames
    frame_rate = cap.get(5)  # frame rate
    if frame_rate not in range(1, 50):  # make it more logical
        frame_rate = 25
    rate_divisor = max(math.ceil(frame_rate / required_frames_per_sec), 1)
    video_duration = round(float(num_frame) / float(frame_rate), 2)

    video_frame_folder = video_name + "_frame"
    while (os.path.isdir(video_frame_folder) == True):
        video_frame_folder = video_frame_folder + "_1"
    os.mkdir(video_frame_folder)

    while (cap.isOpened()):
        frame_id = cap.get(1)
        ret, frame_ur = cap.read()
        if (ret != True):
            break
        frame = imutils.rotate_bound(frame_ur, rotation_angle)
        if (frame_id % rate_divisor == 0):
            filename = video_frame_folder + "/frame%d.jpg" % total_frames_created
            total_frames_created += 1
            cv2.imwrite(filename, frame)

    cap.release()
    return (video_name, video_frame_folder, video_duration, total_frames_created)

def find_video_rotation__(video_path):

    cmd = 'ffmpeg -i %s -f null -' % video_path
    p = subprocess.Popen(cmd.split(" "), stderr=subprocess.PIPE, close_fds=True)
    stdout, stderr = p.communicate()

    video_metadata_list = re.split('=|\s+', str(stderr))

    try:
        rotation_index = video_metadata_list.index("rotate")
        rotation = int(re.split("[^0-9]+", video_metadata_list[rotation_index + 2])[0])
    except:
        rotation = 0

    return rotation
