import argparse
import glob
import time

from predictor import Predictor


def inference(video_dir):
    predictor = Predictor()
    predictor.build_model()
    videos_src = glob.glob(video_dir + "*mp4")
    for video_src in videos_src:
        nsfw_final_weighted_score, nsfw_confidence_level = predictor.predict(video_src)
        print(video_src,nsfw_final_weighted_score, nsfw_confidence_level)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--video_dir",
                        help="video_src",
                        default="dataset/sample_videos/")
    args = parser.parse_args()

    video_dir = args.video_dir

    inference(video_dir)
