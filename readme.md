# Brand Safety - Nude Content Detection
### Description
Our web services are presents on a multitude of websites display video content to the end users. As the company envision to make the services driven end to end by Artificial Intelligence, it is imperative to leverage the AI capabilities to bar obscene contents from going into the database. The Nudity Detection API will detect if a video contains nude or sexual content and is safe or unsafe to be displayed in the web services.
### Prerequisites
Basic Libraries
1. python 
2. numpy (1.14)
3. scikit-image (0.14)
4. tensorflow ( 1.10)
5. imutils (0.5)

Refer requirement.txt for complete dependencies.

### Run

Put weights in the `model_weigths` repository. Weights can be downloaded from - https://drive.google.com/open?id=1YMHJyBNcv4mkDs74kieJS4OlQbzumaw8

Put safe and unsafe images inside dataset/sample_set folders in the safe and unsafe folders respectively for training. 

Put videos in dataset/sample_videos for inference. 

To build the predictor instance

To begin the training - 
`python train.py`

To run the model on videos - 
`python inference.py`
### Integration

Add the following line in the code to integrate into other API
To import the class
`from predictor import Predictor`

`predictor = Predictor()`

`predictor.build_model()`

To run the predictor on a video

`nsfw_final_weighted_score, nsfw_confidence_level = predictor.predict(video_src)`


### Result Descriptipn
`nsfw_confidence_level` is an integer score with a from 0 to 3. Score 0 is safe and 1,2 and 3 are unsafe with 1 with least confidence and 3 with most confidence. Empirically, pornographic video content gets a score of 3 and sexually suggestive content gets a score of 1 or 2.

`nsfw_final_weighted_score` is weighted score of different nsfw scores as it is generally high for NSFW and low or 0 for Non-NSFW videos.