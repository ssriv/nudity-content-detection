import os
import shutil

import numpy as np
import tensorflow as tf

from model import NudityDetectionModel, InputType
from utils.image_utils import IMAGE_LOADER_TENSORFLOW, IMAGE_LOADER_YAHOO, create_tensorflow_image_loader, \
    create_yahoo_image_loader
from utils.video_utils import extract_frames_from_video

class Predictor():

    def __init__(self):

        self.model = None
        self.model_weights = None
        self.input_type = None
        self.image_loader = None
        self.required_frames_per_sec = None
        self.session = None
        self.fn_load_image = None

    def build_model(self):

        self.model = NudityDetectionModel()

        self.model_weights = "model_weigths/open_nsfw-weights.npy"

        input_type = InputType.TENSOR.name.lower()
        self.input_type = InputType[input_type.upper()]

        self.image_loader = IMAGE_LOADER_YAHOO

        self.required_frames_per_sec = 1

        self.session = tf.Session()

        if self.input_type == InputType.TENSOR:
            if self.image_loader == IMAGE_LOADER_TENSORFLOW:
                self.fn_load_image = create_tensorflow_image_loader(tf.Session(graph=tf.Graph()))
            elif self.image_loader == IMAGE_LOADER_YAHOO:
                self.fn_load_image = create_yahoo_image_loader()
        elif input_type == InputType.BASE64_JPEG:
            import base64
            self.fn_load_image = lambda filename: np.array([base64.urlsafe_b64encode(open(filename, "rb").read())])

        self.model.build(weights_path=self.model_weights, input_type=self.input_type,trainable=False)

        self.session.run(tf.global_variables_initializer())

    def prediction(self, video_frame_folder):

        nsfw_confidence_level = 0

        # Threshold values
        nsfw_score_threshold_min = 0.50
        nsfw_score_threshold_medium = 0.80
        nsfw_score_threshold_high = 0.90
        nsfw_score_threshold_very_high = 0.95

        # total count of frames under each threshold values
        total_low_nsfw_frame = 0
        total_medium_nsfw_frame = 0
        total_high_nsfw_frame = 0
        total_very_high_nsfw_frame = 0

        # scores with respect to threshold
        nsfw_score_very_high = 0
        nsfw_score_high = 0
        nsfw_score_medium = 0
        nsfw_score_low = 0

        # total score of safe and unsafe frames
        total_sfw_score = 0
        total_nsfw_score = 0

        # total safe, unsafe and all frames
        total_sfw_frames = 0
        total_nsfw_frames = 0
        total_frames = 0

        # continuous frame
        nsfw_score_threshold_cont = 0.80
        is_previous_frame_nsfw = False
        current_continuous_nsfw_score_count = 0
        current_continuous_nsfw_score = 0
        total_cont_frames_above_cont_thres = 0
        total_cont_scores_above_cont_thres = 0
        total_cont_num_frames_above_cont_thres = 0
        continuous_min_frame_count = 10

        next_frame_exist = True
        frame_num = 0
        image_path = video_frame_folder + "/" + "frame" + str(frame_num) + ".jpg"

        while next_frame_exist:

            assert (os.path.isfile(image_path) == True)
            image = self.fn_load_image(image_path)

            predictions = \
                self.session.run(self.model.predictions,
                                 feed_dict={self.model.input: image})
            current_sfw_score = predictions[0][0]
            current_nsfw_score = predictions[0][1]
            total_frames += 1

            if current_nsfw_score >= nsfw_score_threshold_min:

                if is_previous_frame_nsfw == False and current_nsfw_score >= nsfw_score_threshold_cont:
                    current_continuous_nsfw_score_count = 1
                    current_continuous_nsfw_score = current_nsfw_score
                elif current_nsfw_score >= nsfw_score_threshold_medium:
                    current_continuous_nsfw_score_count += 1
                    current_continuous_nsfw_score += current_nsfw_score

                total_nsfw_score += current_nsfw_score
                total_nsfw_frames += 1

                if current_nsfw_score >= nsfw_score_threshold_very_high:
                    total_very_high_nsfw_frame += 1
                    nsfw_score_very_high += current_nsfw_score
                elif current_nsfw_score >= nsfw_score_threshold_high:
                    total_high_nsfw_frame += 1
                    nsfw_score_high += current_nsfw_score
                elif current_nsfw_score >= nsfw_score_threshold_medium:
                    total_medium_nsfw_frame += 1
                    nsfw_score_medium += current_nsfw_score
                else:
                    total_low_nsfw_frame += 1
                    nsfw_score_low += current_nsfw_score
                if current_nsfw_score < nsfw_score_threshold_cont:
                    if current_continuous_nsfw_score_count >= continuous_min_frame_count:
                        total_cont_frames_above_cont_thres += 1
                        total_cont_num_frames_above_cont_thres += current_continuous_nsfw_score_count
                        total_cont_scores_above_cont_thres += current_continuous_nsfw_score
                    current_continuous_nsfw_score_count = 0
                    current_continuous_nsfw_score = 0
                if is_previous_frame_nsfw == False:
                    is_previous_frame_nsfw = True
            else:
                total_sfw_score += current_sfw_score
                total_sfw_frames += 1
                if is_previous_frame_nsfw == True:
                    is_previous_frame_nsfw = False

            frame_num += 1
            image_path = video_frame_folder + "/" + "frame" + str(frame_num) + ".jpg"
            if (os.path.isfile(image_path) == False):
                next_frame_exist = False

        # to delete frame folder
        shutil.rmtree(video_frame_folder)

        # The avg score of NSFW classified frames - considering only frames with high confidence score
        total_frame_above_medium = total_very_high_nsfw_frame + total_high_nsfw_frame + total_medium_nsfw_frame
        total_score_above_medium = nsfw_score_very_high + nsfw_score_high + nsfw_score_medium

        avg_above_medium_nsfw_score_per_above_medium = 0 if total_frame_above_medium == 0 else round(
            total_score_above_medium / total_frame_above_medium, 2
        )

        # avg score of only continuous frames which are nsfw per continous avg frames
        avg_cont_score_nsfw = 0 if total_cont_num_frames_above_cont_thres == 0 else round(
            total_cont_scores_above_cont_thres / total_cont_num_frames_above_cont_thres, 2)

        # Assuming that frames above 80 are highly probable to be NSFW, this score, will give the ratio of number of
        # frames with above 80 to num of frames above 50 score
        ratio_nsfw_pred_med_to_all_nsfw = 0 if total_nsfw_frames == 0 else round(
            total_frame_above_medium / total_nsfw_frames, 2)

        nsfw_final_weighted_score = round(
            (
                    5 * avg_cont_score_nsfw + avg_above_medium_nsfw_score_per_above_medium + ratio_nsfw_pred_med_to_all_nsfw) / 7,
            2)

        if avg_above_medium_nsfw_score_per_above_medium != 0:
            nsfw_confidence_level = 1
        if avg_cont_score_nsfw != 0:
            nsfw_confidence_level = 1
        if ratio_nsfw_pred_med_to_all_nsfw != 0:
            nsfw_confidence_level = 1
        if avg_above_medium_nsfw_score_per_above_medium >= .90 and avg_cont_score_nsfw >= 0.80:
            nsfw_confidence_level = 3
        elif avg_above_medium_nsfw_score_per_above_medium >= .80 and avg_cont_score_nsfw != 0:
            nsfw_confidence_level = 2

        return nsfw_final_weighted_score, nsfw_confidence_level

    def predict(self, video_src):

        video_name, video_frame_folder, video_duration, total_frames_created = extract_frames_from_video(video_src,
                                                                                                         self.required_frames_per_sec)
        nsfw_final_weighted_score, nsfw_confidence_level = self.prediction(video_frame_folder)

        return nsfw_final_weighted_score, nsfw_confidence_level
